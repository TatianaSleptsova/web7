$(document).ready(function() {
  $('.gallery').slick({
    arrows: true,
    dots: true,
   slidesToShow: 4,
   slidesToScroll: 4,
    autoplay: true,
   
      responsive: [
          {
              breakpoint: 769,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  
              }
          },
      ]
  });
});
